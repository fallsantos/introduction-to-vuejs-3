let app = new Vue({
    el: '#app',
    data: {
        title: 'Introdução ao VueJS 3',
        name: 'Flávio',
        lastName: 'Santos ',
        date_now: new Date().toLocaleString(),
        teste: 'teste',
        products: [
            {
                id: 1,
                title: 'Imagem 01',
                description: 'Descrição da imagem 01',
                image: './assets/images/img01.jpg',
                stars: 3
            },
            {
                id: 2,
                title: 'Imagem 02',
                description: 'Descrição da imagem 02',
                image: './assets/images/img02.jpg',
                stars: 1
            },
            {
                id: 3,
                title: 'Imagem 03',
                description: 'Descrição da imagem 03',
                image: './assets/images/img01.jpg',
                stars: 5
            },
            {
                id: 4,
                title: 'Imagem 04',
                description: 'Descrição da imagem 04',
                image: './assets/images/img02.jpg',
                stars: 1
            }
        ],
        cart: [],
        //textColor: '#000',
        //bgColor: '#fff'
        styles: {
            backgroundColor: '#fff',
            color: '#000'
        },
        themeBlack: false
    },
    methods: {
        addCart (product) {
            this.cart.push(product)
        },
        inCart (product) {
            return this.cart.indexOf(product) != -1
        },
        removeCart(product) {
            this.cart = this.cart.filter((item, index) => product != item )
        },
        toogleTheme () {
            this.themeBlack = !this.themeBlack

            if(this.themeBlack){
                this.styles.backgroundColor = '#000'
                this.styles.color = '#fff'
            }else{
                this.styles.backgroundColor = '#fff'
                this.styles.color = '#000'
            }
        }
    },

    computed: {
        fullName() {
            return this.name + ' ' + this.lastName
        }
    }
})